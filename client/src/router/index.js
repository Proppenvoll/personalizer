import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/uebersicht",
    name: "Overview",
    component: () =>
      import(/* webpackChunkName: "overview" */ "../views/overview.vue"),
  },
  {
    path: "/einstellungen",
    name: "Settings",
    component: () =>
      import(/* webpackChunkName: "settings" */ "../views/settings.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
