import { createApp } from "vue";
import App from "./app.vue";
import "./index.css";
import router from "./router";

// import axios from "axios";

// axios.interceptors.response.use(
//   (res) => res,
//   (err) => {
//     if (err.response.status !== 401) {
//       return Promise.reject(err);
//     }
//     axios.get("/token")
//   }
// );

createApp(App)
  .use(router)
  .mount("#app");
