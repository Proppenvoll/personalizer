import AppHeader from "./app-header.vue";
import ThemeToggle from "./theme-toggle.vue";
import AppCard from "./app-card.vue";
import AppRegister from "./app-register.vue";
import AppLogin from "./app-login.vue";
import AppModal from "./app-modal.vue";
import SectionUser from "./section-user.vue";
import SectionRole from "./section-role.vue";
import SectionPermission from "./section-permission.vue";
import ButtonRemove from "./button-remove.vue";

export {
  AppHeader,
  ThemeToggle,
  AppCard,
  AppRegister,
  AppLogin,
  AppModal,
  SectionUser,
  ButtonRemove,
  SectionPermission,
  SectionRole,
};
