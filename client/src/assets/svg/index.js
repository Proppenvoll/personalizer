import SvgChartPie from "./chart-pie.vue";
import SvgBeaker from "./beaker.vue";
import SvgMoon from "./moon.vue";
import SvgSun from "./sun.vue";
import SvgUsers from "./users.vue";

export { SvgChartPie, SvgBeaker, SvgMoon, SvgSun, SvgUsers };
