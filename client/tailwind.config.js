module.exports = {
  purge: ["./public/index.html", "./src/**/*.vue"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ["Lato", "sans-serif"],
    },
    extend: {
      colors: {
        primary: "var(--clr-primary)",
        secondary: "var(--clr-secondary)",
        background: "var(--clr-background)",
        "on-background": "var(--clr-on-background)",
        surface: "var(--clr-surface)",
        "on-surface": "var(--clr-on-surface)",
        error: "var(--clr-error)",
        overlay: "var(--clr-overlay)",
        //'on-primary': 'var(--clr-on-primary)',
      },
      fontFamily: {
        // sans: ["Lato", "sans-serif"],
        handwritten: ["Cookie", "serif"],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
