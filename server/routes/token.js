import bcrypt from "bcryptjs";
import express from "express";
import jwt from "jsonwebtoken";
import validator from "validator";

import { account } from "../models/index.js";
import { validation } from "../middlewares/index.js";
import { createAccessToken, createRefreshToken } from "../utils/token.js";

const router = express.Router();

router.post("/", validation({ email: "email", password: "password" }), login);

router.get("/", refreshRefreshToken);

// router.post(
//   "/refreshToken",
// TODO: validation for cookies
// cookieValidation({ jwt: "refreshToken" }),
// refreshRefreshToken
// );

async function login(req, res) {
  let doc;
  try {
    doc = await account.findOne({ email: req.body.email });
    if (!(await bcrypt.compare(req.body.password, doc.password))) {
      return res.status(400).json({ msg: "Provide valid credentials" });
    }
  } catch (err) {
    console.log(err);
    return res.status(400).json({ msg: "Provide valid credentials" });
  }

  let accessToken;
  try {
    doc.refreshToken = await createRefreshToken(doc.id);
    accessToken = await createAccessToken(doc.id);
    await doc.save();
  } catch (err) {
    console.log(err);
    return res.status(500).json({ msg: "Something went wrong" });
  }

  res.cookie("refreshToken", doc.refreshToken, {
    secure: true,
    httpOnly: true,
  });

  res.json({
    accessToken,
    // user: { name: user.name, id: user.id, email: user.email },
  });
}

function refreshRefreshToken(req, res) {
  if (!req.cookies.refreshToken)
    return res
      .status(401)
      .json({ msg: "Provide valid refresh token in cookie" });

  if (!validator.isJWT(req.cookies.refreshToken))
    return res.status(401).json({ msg: "Cookie is not a jwt token" });

  try {
    jwt.verify(
      req.cookies.refreshToken,
      process.env.JWT_REFRESH_SECRET,
      async (err, decoded) => {
        if (err) throw err;

        let doc;
        try {
          doc = await account.findById(decoded.id, "refreshToken");
        } catch (err) {
          return res.json({ msg: "User not found" });
        }

        // // Only supply if refresh token from db is equal
        if (doc.refreshToken !== req.cookies.refreshToken)
          return res.json({
            msg: "Invalid refresh token provided. Please login",
          });

        const newAccessToken = await createAccessToken(doc.id);
        const newRefreshToken = await createRefreshToken(doc.id);

        doc.refreshToken = newRefreshToken;
        try {
          await doc.save();
        } catch (err) {
          // TODO: message and status code
          return res.status(500).json({ msg: "Couldnt save" });
        }

        res.cookie("refreshToken", doc.refreshToken, {
          secure: true,
          httpOnly: true,
        });

        return res.json({
          accessToken: newAccessToken,
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.json({ msg: "ab" });
  }

  // let doc;
  // try {}
  // compare refresh tokens
  // if equal create new one and access token
  // res.json({ msg: req.cookies.refreshToken });
}

// const refreshToken = req.body.refreshToken;
// let decoded;
// try {
//   decoded = jwt.verify(refreshToken, process.env.JWT_REFRESH_SECRET);
// } catch (err) {
//   return res.json({ msg: "Refresh token not valid" });
// }

// let doc;
// try {
//   doc = await userModel.findById(decoded.id, "refreshToken");
// } catch (err) {
//   return res.json({ msg: "User not found" });
// }

// // Only supply if refresh token from db is equal
// if (doc.refreshToken !== refreshToken)
//   return res.json({ msg: "Invalid refresh token provided. Please login" });

// const newAccessToken = createAccessToken(doc.id);
// const newRefreshToken = createRefreshToken(doc.id);

// doc.refreshToken = newRefreshToken;
// try {
//   await doc.save();
// } catch (err) {
//   // TODO: message and status code
//   return res.status(500).json({ msg: "Couldnt save" });
// }

// return res.json({
//   accessToken: newAccessToken,
//   refreshToken: newRefreshToken,
// });

export default router;
