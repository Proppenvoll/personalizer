import express from "express";

import { account, token } from "./index.js";
import { checkAuth } from "../middlewares/authorization.js";
// import token from "./token.js";

const router = express.Router();

router.use("/accounts", account);
router.use("/token", token);

router.get("/", checkAuth, (req, res) => {
  const resBody = {
    msg: "Welcome to Personalizer!",
  };
  const publicLinks = {
    login: "/token",
    register: "/accounts",
  };
  const privateLinks = {
    ...publicLinks,
    users: "/users",
    roles: "/roles",
    permissions: "/permissions",
  };

  if (req.jwtContent) return res.json(resBody, privateLinks);

  res.json(resBody, publicLinks);
});

export default router;
