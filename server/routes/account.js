import bcrypt from "bcryptjs";
import express from "express";

import { account } from "../models/index.js";
import { validation } from "../middlewares/index.js";
import auth, { checkAuth } from "../middlewares/authorization.js";
import { createAccessToken, createRefreshToken } from "../utils/token.js";

const router = express.Router();

// TODO: remove
// router.get("/", (req, res) => {
//   console.log(req.cookies.refreshToken);
//   account.find().then((doc) => {
//     res.json(doc);
//   });
// });

router.post(
  "/",
  validation({
    email: "email",
    password: "password",
    invitationCode: "invitationCode",
  }),
  createAccount
);

router.get("/", checkAuth, auth, getAccountInfo);
// router.get("/:id", getAccountInfo);
// router.put("/:id", getAccountInfos);
// router.update("/:id", updateAccountInfo);
router.delete("/:id", deleteAccount);

// function getAccountInfo(req, res) {
//   // Change to provided id by jwt
//   account
//     .findById(req.params.id)
//     .then((doc) => {
//       res.json({ doc });
//     })
//     .catch((err) => {
//       console.log(err);
//       res.status(404).send();
//     });
// }

function getAccountInfo(req, res) {
  // Change to provided id by jwt
  account
    .findById(req.jwtContent.id, "-password -refreshToken")
    .then((doc) => {
      res.json({ email: doc.email });
    })
    .catch((err) => {
      console.log(err);
      res.status(404).send();
    });
}

// TODO:
function deleteAccount(req, res) {
  account
    .findByIdAndDelete(req.jwtContent.id)
    .then(() => {
      res.status(204).send();
    })
    .catch((err) => {
      console.log(err);
      return res.status(404).send();
    });
}

async function createAccount(req, res) {
  // Check if invation code is correct
  if (req.body.invitationCode !== process.env.INVITATION_CODE)
    return res.status(400).json({ msg: "Wrong invitation code" });

  // Check if email is taken
  try {
    if (await account.findOne({ email: req.body.email }))
      return res.status(400).json({ msg: "Email taken" });
  } catch (err) {
    console.log(err);
    return res.status(400).json({ msg: "Something went wrong" });
  }

  let salt;
  try {
    salt = await bcrypt.genSalt(10);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ msg: "Something went wrong" });
  }

  let hashedPassword;
  try {
    hashedPassword = await bcrypt.hash(req.body.password, salt);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ msg: "Something went wrong" });
  }

  const newAccount = new account({
    email: req.body.email,
    password: hashedPassword,
  });

  let accessToken;
  let refreshToken;
  try {
    accessToken = await createAccessToken(newAccount.id);
    refreshToken = await createRefreshToken(newAccount.id);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ msg: "Something went wrong" });
  }

  newAccount.refreshToken = refreshToken;

  try {
    await newAccount.save();
  } catch (err) {
    console.log(err);
    return res.status(400).json({ msg: "Email already taken" });
  }

  res.cookie("refreshToken", refreshToken, {
    secure: true,
    httpOnly: true,
  });

  res.json({ accessToken });
}

export default router;
