import validator from "validator";

/**
 * Makes sure that each field in body is set and valid
 *
 * @param {object} fields Key describes type for valdiation check; value provides name which is used in req.body. Value can be an array.
 */
export default function validation(fields) {
  return async (req, res, next) => {
    try {
      // console.log(fields);
      const fieldsSkeleton = {};
      // Provide all fields
      let isFieldEmpty = false;
      // Get all body values and set undefined fields to empty string
      for (let i of Object.values(fields)) {
        // If array is provided, iterate over it
        if (Array.isArray(i)) {
          for (let j of i) {
            fieldsSkeleton[j] = "";
            if (!req.body[j]) {
              isFieldEmpty = true;
            }
          }
        } else {
          fieldsSkeleton[i] = "";
          if (!req.body[i]) {
            isFieldEmpty = true;
          }
        }
      }

      if (isFieldEmpty) {
        return res
          .status(400)
          .json({ msg: "Provide all fields", fields: fieldsSkeleton });
      }

      // Validate input
      // TODO: check in req
      for (let type in fields) {
        switch (type) {
          case "email":
            if (Array.isArray(fields[type])) {
              for (let j of fields[type]) {
                if (!validator.isEmail(req.body[j]))
                  return res
                    .status(400)
                    .json({ msg: j + " Email address not valid" });
              }
            } else {
              if (!validator.isEmail(req.body[type]))
                return res.status(400).json({ msg: "Email address not valid" });
            }
            break;

          case "password":
            if (!validator.isStrongPassword(req.body[type]))
              return res.status(400).json({
                msg:
                  "Password min length is 8, at least one lowercase, uppercase, number and symbol",
              });
            break;

          case "name":
            if (!validator.isLength(req.body[type], { min: 2, max: 50 }))
              return res.status(400).json({
                msg:
                  req.body[type] +
                  " should be at least 2 and maximum 50 characters long",
              });
            break;

          case "title":
            if (
              !validator.isLength(req.body[type], { min: undefined, max: 50 })
            )
              return res.status(400).json({
                msg: fields[type] + " should be maximum 50 characters long",
              });
            break;

          case "jwt":
            console.log(req.body[fields[type]]);
            if (!validator.isJWT(req.body[fields[type]]))
              return res.status(400).json({
                msg: fields[type] + " should be a jwt",
              });
            break;

          case "invitationCode":
            if (
              !validator.isNumeric(req.body[fields[type]], { no_symbols: true })
            )
              return res.status(400).json({
                msg: fields[type] + " should be a number",
              });
            break;

          default:
        }
      }

      // TODO: additional sanitization?

      // Attach fields key/value pairs to req
      // for (let i in fields) {
      //   req[i] = fields[i];
      // }
      next();
    } catch (err) {
      res.status(500).json({ msg: err });
    }
  };
}

function cookieValidation(fields) {
  return async (req, res, next) => {};
}
