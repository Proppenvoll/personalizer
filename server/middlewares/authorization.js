import jwt from "jsonwebtoken";
import validator from "validator";

/**
 * If authorization header is provided and valid, set jwtContent in req.
 */
export function checkAuth(req, _res, next) {
  // console.log(req.cookies);

  // Header check
  if (req.headers.authorization) {
    // Get rid of Bearer prefix as the only authentification type is jwt.
    const token = req.headers.authorization.split(" ")[1];
    // if (!validator.isJWT(token)) return res.status(401).json({msg: "Authorization token not valid"});
    try {
      // validate here
      const decoded = jwt.verify(token, process.env.JWT_ACCESS_SECRET);
      req.jwtContent = decoded;
    } catch (err) {
      req.jwtContent = null;
    }
  }

  // Cookie check
  // if (req.cookies.refreshToken) {
  //   if (!validator.isJWT(req.cookies.refreshToken)) return res.status(401).json({msg: "Cookie is not a jwt token"});
  //   jwt.verify(req.cookies.refreshToken, process.env.JWT_REFRESH_SECRET, (err, decoded) => {
  //     if (err) return res.status(401).json({msg: "Refresh token not valid"}, {login: "/token"})
  //   })
  // }
  next();
}

/*
 * Makes sure that the user is authorized. Use checkAuth() beforehand to set
 * req.jwtContent.
 */
export default function auth(req, res, next) {
  if (!req.headers.authorization)
    return res.status(401).json({ msg: "Provide access token" });

  if (!req.jwtContent)
    return res.status(401).json({ msg: "Provide valid access token" });

  next();
}
