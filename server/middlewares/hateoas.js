/**
 * Extends res.json(body) to res.json(body, links). Self link is automatically
 * applied.
 */
export default function hateoas(req, res, next) {
  const oldJson = res.json;

  res.json = (data, links) => {
    const finalLinks = {
      ...data.links,
      // TODO: Removes /api/ . Better way to do this?!
      self: req.originalUrl.replace(/^\/\w+/, ""),
      ...links,
    };

    if (Array.isArray(data)) {
      data = {
        arr: [...data],
        links: finalLinks,
      };
    } else {
      data = {
        ...data,
        links: finalLinks,
      };
    }
    res.json = oldJson;
    return res.json(data);
  };
  next();
}
