import jwt from "jsonwebtoken";

function createAccessToken(id) {
  return new Promise((resolve, reject) => {
    jwt.sign(
      { id },
      process.env.JWT_ACCESS_SECRET,
      {
        expiresIn: "15m",
      },
      (err, token) => {
        if (err) {
          reject(err);
        }
        resolve(token);
      }
    );
  });
}

function createRefreshToken(id) {
  return new Promise((resolve, reject) => {
    jwt.sign(
      { id },
      process.env.JWT_REFRESH_SECRET,
      {
        // expiresIn: "15m",
      },
      (err, token) => {
        if (err) {
          reject(err);
        }
        resolve(token);
      }
    );
  });
}

export { createAccessToken, createRefreshToken };
