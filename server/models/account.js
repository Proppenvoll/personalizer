import mongoose from "mongoose";

const schema = mongoose.Schema(
  {
    email: {
      type: String,
      unique: true,
    },
    password: {
      type: String,
    },
    refreshToken: String,
  },
  { versionKey: false }
);

const account = mongoose.model("account", schema);

export default account;
