import mongoose from "mongoose";

const schema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  phoneNumber: String,
  // roles
});

const user = mongoose.model("user", schema);

export default user;
