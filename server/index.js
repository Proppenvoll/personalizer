import express from "express";
import mongoose from "mongoose";
import cookieParser from "cookie-parser";

import api from "./routes/api.js";
import { hateoas } from "./middlewares/index.js";

const app = express();

// Middlewares
app.use(express.json());
app.use(cookieParser());
app.use(hateoas);

// Routes
app.use("/api", api);

app.use(express.static("static"));

mongoose
  .connect(
    `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@db:27017`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }
  )
  .then(() => {
    console.log("Connected to database");
    app.listen(8000);
  })
  .catch((err) => console.log(err));
