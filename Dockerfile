FROM node:15.8-alpine AS client-build

WORKDIR /app

COPY ./client/package*.json ./

RUN npm install

COPY ./client .

RUN npm run build


FROM node:15.8-alpine

WORKDIR /usr/src/app

COPY ./server/package*.json ./

RUN npm ci --only=production

COPY ./server ./

# COPY ./client/dist ./static
COPY --from=client-build /app/dist ./static

EXPOSE 8000

USER node

CMD ["npm", "run", "start"]
